module.exports = async (ctx, next) => {            
    if (ctx.state.needHtml) {        
        ctx.body = '<div>Please, use webpack proxy server</div>';
        return;
    }    
    await next();
}
    