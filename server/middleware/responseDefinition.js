module.exports = async (ctx, next) => {        
    ctx.state.needHtml = !ctx.request.headers ||
                         !ctx.request.headers.accept ||
                         !(
                             ctx.request.headers.accept.includes('json') ||
                             ctx.request.headers.accept.includes('image')
                         );

    await next();
}
    