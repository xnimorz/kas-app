const Koa = require('koa');
const koaBody = require('koa-body');

const router = require('./routes');

const app = new Koa();

app.use(async (ctx, next) => {
    var start = new Date;
    await next();
    var ms = new Date - start;
    ctx.set('X-Response-Time', ms + 'ms');
});

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        console.error(err);
        ctx.body = { reason: err.message };
        ctx.status = err.status || 500;
    }
});

app.use(async (ctx, next) => {
    var start = new Date;
    await next();
    var ms = new Date - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});

app.use(koaBody({
    multipart: true,
    urlencoded: true,
}));

app.use(router.routes());

app.on('error', function(err){
    console.error('server error - ', err);
});

var server = app.listen(3030);

console.log('Server listening on 0:0:0:0:3030');