const basket = {
    count: 0,
    goods: [],
};

function getStatus() {
    return {
        count: basket.count,
    };
}

function productInBasket(id) {
    const product = basket.goods.find(item => item.id === id);
    if (product) {
        return product.count;
    }

    return 0;
}

function addToBasket(id) {
    let product = basket.goods.find(item => item.id === id);
    if (product) {
        product.count++;
        basket.count++;
        return product;
    } 
    product = {id, count: 1};
    basket.goods.push(product);
    basket.count++;
    return product;
}

function removeFromBasket(id) {
    let product = basket.goods.find(item => item.id === id);
    if (product) {
        product.count--;
        basket.count--;
        if (product.count === 0) {
            basket.goods = basket.goods.filter(item => item.id !== id);            
        }
        return product;
    } 

    throw {status: 404};
}

function getBasket() {
    return basket;
}

module.exports = {
    getBasket,
    removeFromBasket,
    addToBasket,
    productInBasket,
    getStatus,
};