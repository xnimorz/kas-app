const imagesModel = require('../resources/images.json');

let imageIndex = Math.max(...Object.keys(imagesModel).map(id => Number(id)));

function getImage(id) {
    return imagesModel[id];
}

function newImage(extension) {
    imageIndex++;
    const imageName = `${imageIndex}${extension}`;
    imagesModel[imageIndex] = imageName;
    return {id: imageIndex, name: imageName};
}

module.exports = {
    newImage,
    getImage,
};