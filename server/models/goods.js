const goodsModel = require('../resources/goods.json');

function getGoodsByCategory(category) {
    /*
    * Здесь используется топорный метод поиска товара по категории, так как товаров достаточно мало. 
    * Если говорить о больших объемах данных, то:
    * 1) В случае "хранить без базы" — стоит перед работой сервера общий массив товаров преобразовать в объект, который хранит товары в слуедующем виде:
    *  {[categoryId]: [goodsByCategoryId]}
    * 2) В случае с базой — строим индекс по categoryId
    * Но в нашем случае товаром мало, поэтому используем простой фильтр
    */
    return goodsModel.filter(item => item.categoryId === category);
}

function getGoods({page = 0, limit = 5}, category) {
    let goods = goodsModel;
    if (category) {
        goods = getGoodsByCategory(category);        
    }

    if (!goods || goods.length === 0) {
        throw {status: 404};
    }

    return {
        pagination: {
            count: Math.floor(goods.length / limit) + (goods.length % limit ? 1 : 0),
            current: page,
            limit,
        },
        items: goods.slice((page - 1) * limit, (page - 1) * limit + limit),
    };    
}

function getProduct(id) {
    /*
    * Аналогично, так как товаров мало — делаем простой find на javascript. 
    * Если бы товаров было очень много и данные нужно держать в памяти, то воспользоваться нужно: 
    * 1) Или превратить список товаров в хешмапу (javascript объект) — доступ за O(1) (но дублирование данных)
    * 2) Или использовать бинарный поиск O(logN)
    * Если база данных — то данный id должен являтья primaryKey
    */
    return goodsModel.find(item => item.itemId === id);
}

function setProductById(id, {categoryId, name, price, description, imageId}) {
    /**
     * Также этот метод можно написать как:
     * goodsModel.map(item => item.itemId === id ? {itemId: item.itemId, categoryId, name, description, imageId} : item)
     * Этот метод более функционален, возвращает новый массив, но в тоже время мы получаем большие затраты на перекладывание объектов просто так. 
     * Что для сервера излишне
     */

     
    const product = getProduct(id);
    if (!product) {
        throw {status: 404};
    }

    const category = parseInt(categoryId, 10);
    if (isNaN(category)) {
        throw {status: 400};
    }

    product.categoryId = category;
    product.name = name;
    product.price = String(price).replace(/\D/g, '');
    product.description = description;
    product.imageId = imageId;

    return product;
}

module.exports = {
    setProductById,
    getProduct,
    getGoods,
};