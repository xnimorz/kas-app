const categoriesModel = require('../resources/categories.json');

function getCategories() {
    return categoriesModel;
}

function getCategoryById(id) {
    return categoriesModel[id];
}

module.exports = {
    getCategories,
    getCategoryById,
};