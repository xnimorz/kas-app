const Router = require('koa-router');
const serve = require('koa-static');
const send = require('koa-send');
const fs = require('fs');
const path = require('path');

const responseDefinitionMiddleware = require('./middleware/responseDefinition');
const htmlServeMiddleware = require('./middleware/htmlServe');

const categoriesModel = require('./models/categories');
const goodsModel = require('./models/goods');
const imagesModel = require('./models/images');
const basketModel = require('./models/basket');

const router = Router();
const app = Router();

router.get('categories', (ctx) => {
    ctx.body = categoriesModel.getCategories();
});

router.get('goods', ctx => {
    const page = parseInt(ctx.request.query.page || 1, 10);
    const limit = parseInt(ctx.request.query.limit || 5, 10);

    if (isNaN(page) || isNaN(limit)) {
        ctx.response.status = 400;        
        return;
    }

    const goods = goodsModel.getGoods({page, limit});
    const categories = categoriesModel.getCategories();
    const basket = basketModel.getStatus();

    ctx.body = {
        basket,
        categories: {
            items: categories,
            selected: {id: null},
        },
        goods,
    };
});

router.get('goods/:categoryId', ctx => {
    const page = parseInt(ctx.request.query.page || 1, 10);
    const limit = parseInt(ctx.request.query.limit || 5, 10);    
    const categoryId = parseInt(ctx.params.categoryId, 10);

    if (isNaN(page) || isNaN(limit) || isNaN(categoryId) || limit < 1 || page < 1) {
        ctx.response.status = 400;        
        return;
    }

    const goods = goodsModel.getGoods({page, limit}, categoryId);
    const categories = categoriesModel.getCategories();
    const basket = basketModel.getStatus();
    
    ctx.body = {
        basket,
        categories: {
            items: categories,
            selected: {id: categoryId},
        },
        goods,
    };
});

router.get('product/:id', ctx => {
    const productId = parseInt(ctx.params.id, 10);
    if (isNaN(productId)) {
        ctx.response.status = 404;
        return;
    }

    const product = goodsModel.getProduct(productId);    
    
    if (!product) {
        ctx.response.status = 404;
        return;
    }

    const categories = categoriesModel.getCategories();
    const productsInBasket = basketModel.productInBasket(productId);
    const basket = basketModel.getStatus();

    ctx.body = {
        categories: {
            items: categories,
            selected: {id: null},
        },
        product: product,
        basket: Object.assign({}, basket, {goods: [{id: productId, count: productsInBasket}]}),
    };
});

router.put('product/:id', ctx => {
    const productId = parseInt(ctx.params.id, 10);
    if (isNaN(productId)) {
        ctx.response.status = 404;
        return;
    }

    const product = goodsModel.setProductById(productId, ctx.request.body)    
    ctx.body = product;
});

router.get('basket', ctx => {    
    const basket = basketModel.getBasket();

    const goods = basket.goods.map(item => goodsModel.getProduct(item.id));
    const categories = categoriesModel.getCategories();
    
    ctx.body = {
        categories: {
            items: categories,
            selected: {id: null},
        },
        basket,
        goods: {items: goods}, 
    };
});

router.put('basket/:id', ctx => {
    const productId = parseInt(ctx.params.id, 10);
    if (isNaN(productId)) {
        ctx.response.status = 404;
        return;
    }
    basketModel.addToBasket(productId);
    const basket = basketModel.getBasket();
    
    ctx.body = {
        basket,
    };
});

router.del('basket/:id', ctx => {
    const productId = parseInt(ctx.params.id, 10);
    if (isNaN(productId)) {
        ctx.response.status = 404;
        return;
    }
    basketModel.removeFromBasket(productId);
    const basket = basketModel.getBasket();
    
    ctx.body = {
        basket,
    };
});

/**
 * Так как данные храним в памяти, то и картинки резолвим отдельным ресурсом. Но саму картинку держим на диске. 
 * При "росте" приложения, логично данный вопрос разруливать средставим nginx (webdav)
 */
router.get('image/:id', async ctx => {
    const imageName = imagesModel.getImage(ctx.params.id);
    if (!imageName) {
        ctx.response.status = 404;
        return;
    }

    const dat = path;
    
    await send(ctx, `./server/resources/images/${imageName}`);
});

router.post('image', async ctx => {
    const file = ctx.request.body.files.file;
    const reader = fs.createReadStream(file.path);
    const image = imagesModel.newImage(path.extname(file.name));
    const stream = fs.createWriteStream(path.join('./server/resources/images', image.name));
    reader.pipe(stream);
    console.log('uploading %s -> %s', file.name, stream.path);

    ctx.body = image;
});

app.use('/', responseDefinitionMiddleware, htmlServeMiddleware, router.middleware());

app.use('/static', serve('./build'));

module.exports = app;
