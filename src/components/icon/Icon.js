import React from 'react';
import styled, {css} from 'styled-components';

const Icon = styled.span`
    display: inline-block;
    width: 16px;
    height: 16px;
    background: 0 0 no-repeat;
    vertical-align: middle;
    border: none;
    outline: none;
    text-decoration: none;
    box-sizing: border-box;

    ${
        (props) => {
            if (!props.type) {
                return;
            }
            switch (props.type) {
                case 'cart':
                    return css`
                        width: 34px;
                        height: 32px;
                    `  
                default: 
                    return;
            }            
        }
    }
`

export const Cart = () => {
    return (
        <Icon type='cart'>
            <svg width="34px" height="32px" viewBox="0 0 34 32" version="1.1">                
                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g fill="#fff">
                        <path d="M29,26 L7,26 C6.668,26 6.358,25.835 6.172,25.56 C5.986,25.286 5.948,24.936 6.071,24.628 L7.979,19.858 L6.1,2 L0,2 L0,0 L7,0 C7.512,0 7.941,0.386 7.995,0.895 L9.995,19.896 C10.012,20.057 9.989,20.22 9.929,20.372 L8.477,24 L29,24 L29,26"></path>
                        <path d="M9.204,20.979 L8.796,19.021 L32,14.187 L32,6 L8,6 L8,4 L33,4 C33.552,4 34,4.448 34,5 L34,15 C34,15.474 33.668,15.882 33.204,15.979 L9.204,20.979"></path>
                        <path d="M30,32 C27.794,32 26,30.206 26,28 C26,25.794 27.794,24 30,24 C32.206,24 34,25.794 34,28 C34,30.206 32.206,32 30,32 L30,32 Z M30,26 C28.897,26 28,26.897 28,28 C28,29.103 28.897,30 30,30 C31.103,30 32,29.103 32,28 C32,26.897 31.103,26 30,26 L30,26 Z"></path>
                        <path d="M6,32 C3.794,32 2,30.206 2,28 C2,25.794 3.794,24 6,24 C8.206,24 10,25.794 10,28 C10,30.206 8.206,32 6,32 L6,32 Z M6,26 C4.897,26 4,26.897 4,28 C4,29.103 4.897,30 6,30 C7.103,30 8,29.103 8,28 C8,26.897 7.103,26 6,26 L6,26 Z"></path>
                    </g>
                </g>
            </svg>
        </Icon>
    )
}