import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { write } from '../models/productForm';

const wrapForm = (field, valueConverter) => Component => {
    class FormControl extends PureComponent {
        applyValue = (e) => {
            this.props.write(field, e.value || e.target.value);

            this.props.onChange && this.props.onChange(e);            
        }

        render() {    
            const { onChange, write, value,  ...other } = this.props;        
            return (
                <Component                     
                    {...other} 
                    value={valueConverter ? valueConverter(value) : value}                   
                    onChange={this.applyValue} 
                />
            );
        }
    }

    return connect(state => ({value: state.productForm.data[field]}), {write})(FormControl);
}

export default wrapForm;
