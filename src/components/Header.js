import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { push } from 'react-router-redux';
import { Link } from 'react-router';

import { Cart } from './icon/Icon';
import Select from './Select';
import { CLEAR } from '../constants/modelStates';
import t from '../translations/t';
import Button from './Button';

const Head = styled.div`
    height: 60px;
    display: flex;  
    background-color: #5faace;
    justify-content: space-between;
    box-sizing: border-box;
    padding: 13px 20px;
`;

const CountText = styled.span`
    margin-left: 10px;
`

const Count = styled(Link)`
    font-size: 16px;
    color: #fff;
    text-decoration: none;

    &:hover {
        text-decoration: none;
    }
`

const ButtonLink = Button.withComponent(Link);

class Header extends PureComponent {
    constructor(props, context) {
        super(props, context);

        this.state = {
            categories: this.convertCategoriesToArray(props),
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.categories.status === CLEAR && nextProps.categories !== this.props.categories) {
            this.setState({
                categories: this.convertCategoriesToArray(nextProps),
            });
        }
    }

    convertCategoriesToArray(props) {
        if (props.categories.status !== CLEAR) {
            return [];
        }
        return [{id: 'all', value: t`all.categories`}]
            .concat(
                Object.keys(props.categories.data.items)
                    .map(id => ({
                        id: parseInt(id, 10), 
                        value: props.categories.data.items[id],
                    }))
                );
    }
    
    changeCategory = (e) => {
        if (e.target.value !== 'all') {
            this.props.push(`/goods/${e.target.value}`);
        } else {
            this.props.push('/goods');
        }
    }    

    renderControl() {
        if (this.props.pathname.startsWith('/goods')) {
            return (
                <Select onChange={this.changeCategory} 
                        value={this.props.categories.data.selected.id || 'all'}                        
                        options={this.state.categories}
                />                
            );
        }

        return (
            <ButtonLink to='/goods'>
                {t`to.goods`}
            </ButtonLink>
        )
    }

    render() {
        const { categories, basket } = this.props;

        if (categories.status !== CLEAR || basket.status !== CLEAR) {
            return null;
        }                    

        return (
            <Head>
                {this.renderControl()}                
                <Count to='/basket'>
                    <Cart />
                    <CountText>                    
                        {this.props.basket.data.count}
                    </CountText>
                </Count>
            </Head>
        );
    }
}
 
export default connect(state => ({
    basket: state.basket,
    categories: state.categories,    
    pathname: state.routing.location.pathname,   
}), {
    push,
})(Header);
