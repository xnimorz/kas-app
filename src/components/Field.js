import React, { PureComponent } from 'react';
import styled from 'styled-components';

const Row = styled.div`
    display: flex;
    margin-bottom: 10px;
    font-size: 18px;
`

const Value = styled.div`
    color: #333;
` 

class Field extends PureComponent {
    render() {
        const { children } = this.props;

        return (
            <Row>                
                <Value>
                    {children}
                </Value>
            </Row>
        );        
    }
}

export default Field;