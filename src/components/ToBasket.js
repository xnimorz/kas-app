import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { addToBasket, removeFromBasket } from '../models/basket';
import DummyBasket from './DummyBasket';

class ToBasket extends PureComponent {
    add = () => {
        this.props.addToBasket(this.props.id);
    };

    remove = () => {
        this.props.removeFromBasket(this.props.id);
    };

    render() {
        const stat = this.props.basket.data.goods.find(item => item.id === this.props.id);
        const count = stat ? stat.count : 0;
        return (
            <DummyBasket count={count} add={this.add} remove={this.remove} />
        );
    }
};

export default connect(
    state => ({
        basket: state.basket,   
    }),
    {
        addToBasket,
        removeFromBasket,
    }
)(ToBasket);