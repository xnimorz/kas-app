import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { editProduct } from '../models/product';
import { createForm } from '../models/productForm';
import Field from './Field';
import t from '../translations/t';
import Button from './Button';
import { CLEAR } from '../constants/modelStates';
import Input, { NumberInput } from './Input';
import wrapForm from './FormControl';
import ToBasket from './ToBasket';
import ImageLoader from './ImageLoader';
import Select from './Select';
import Image from './Image';
import numberFormatter from '../converters/numberFormatter';

const Components = {
    name: wrapForm('name')(Input),
    description: wrapForm('description')(Input),
    price: wrapForm('price', value => String(value))(NumberInput),
    imageId: wrapForm('imageId')(ImageLoader),
    categoryId: wrapForm('categoryId', value => parseInt(value, 10))(Select),
};

class ExtendedProduct extends PureComponent {    
    constructor(props, context) {
        super(props, context);

        this.state = {
            isEdit: false,
            categories: this.convertCategoriesToArray(props),
        };  
    }

    toggleEdit = () => {
        if (!this.state.isEdit) {
            this.props.createForm(this.props.product.data);

            this.setState({
                isEdit: true,
            })

            return;
        }

        this.props.editProduct(this.props.product.data.itemId);
        this.setState({
            isEdit: false,
        });
    };

    convertCategoriesToArray(props) {      
        return Object.keys(props.categories.data.items)
            .map(id => ({
                id: parseInt(id, 10), 
                value: props.categories.data.items[id],
            })
        );        
    }

    renderImage(field) {
        if (this.state.isEdit && this.props.productFormStatus === CLEAR) {
            const Control = Components[field];
            return (
                <Control />
            );
        }

        return (
            <Image imageId={this.props.product.data[field]} />
        );
    }

    renderField(field) {
        if (this.state.isEdit && this.props.productFormStatus === CLEAR) {
            const Control = Components[field];
            return (
                <Control />
            );
        }

        if (field === 'price') {
            return numberFormatter(this.props.product.data[field]);
        }
        return this.props.product.data[field];                    
    }   
    
    renderCategory(field) {
        if (this.state.isEdit && this.props.productFormStatus === CLEAR) {
            const Control = Components[field];
            return (
                <Control options={this.state.categories} />
            );
        }

        return this.props.categories.data.items[this.props.product.data[field]];
    }

    render() {
        const { product } = this.props;

        return ( 
            <div>
                <Field>
                    {this.renderField('name')}
                </Field>
                <Field>
                    {this.renderImage('imageId')}
                </Field>
                <Field>
                    {this.renderField('price')}
                </Field> 
                <Field>
                    {this.renderCategory('categoryId')}
                </Field> 
                <Field>
                    {this.renderField('description')}
                </Field>
                <Field>
                    <ToBasket id={product.data.itemId} />
                </Field>  
                <Button onClick={this.toggleEdit}>
                    {this.state.isEdit ? t`save` : t`edit`}
                </Button>
            </div>                                                               
        );
    }
}

export default connect(
    state => ({
        product: state.product,
        categories: state.categories,
        productFormStatus: state.productForm.status,
    }), {
        editProduct,
        createForm,
    }
)(ExtendedProduct);