import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import BasketProduct from './BasketProduct';
import { addToBasket, removeFromBasket } from '../models/basket';
import t from '../translations/t';
import numberFormatter from '../converters/numberFormatter';

const TotalPrice = styled.div`
    color: rgb(17, 20, 206);
    font-size: 18px;
`;

const ProductsContainer = styled.div`
    display: flex;   
    margin-bottom: 20px; 
    flex-wrap: wrap;
`


class BasketList extends PureComponent {
    render() {
        const { basket, goods, addToBasket, removeFromBasket } = this.props;

        // Поиск элементов можно оптимизировать создав computedValue, которое имеет представление {idТовара: данные}
        // Но на текущем объеме данных — выглядит как преждевременная оптимизация.
        return (
            <div>
                <ProductsContainer>
                    {
                        basket.data.goods.map((item) => (
                            <BasketProduct 
                                key={item.id}
                                product={goods.data.items.find(product => product.itemId === item.id)} 
                                count={item.count}
                                add={addToBasket}
                                remove={removeFromBasket}
                            />
                        ))
                    }
                </ProductsContainer>
                <TotalPrice>
                    {t`goods.in.basket`}
                    {basket.data.count}
                    {t`total.price`}
                    {numberFormatter(basket.data.goods.reduce((store, item) => 
                        store + goods.data.items.find(product => product.itemId === item.id).price * item.count, 
                    0))}$
                </TotalPrice>
            </div>
        );
    }
}

export default connect(
    state => ({
        basket: state.basket,
        goods: state.goods,
    }), {
        addToBasket,
        removeFromBasket,
    }
)(BasketList);