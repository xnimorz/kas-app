import styled, { css } from 'styled-components';

export default styled.div`
    width: 200px;
    height: 200px;        

    ${props => css`
        background: url(/image/${props.imageId});        
    `}

    background-position-x: center;
    background-position-y: center;
    background-repeat: no-repeat;    
    background-size: contain;
`;