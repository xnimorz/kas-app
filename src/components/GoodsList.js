import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Product from './Product';

const Container = styled.div`
    margin-top: 10px;
    margin-bottom: 25px;
    display: flex;
    flex-wrap: wrap;
`;

class GoodsList extends PureComponent {
    render() {
        const { goods } = this.props;

        return (
            <Container>
                {
                    goods.map(product => (
                        <Product key={product.itemId} product={product} />                            
                    ))
                }
            </Container>
        );
    }
}

export default GoodsList;