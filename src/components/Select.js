import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Button from './Button';

const SelectTag = styled.select`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: pointer;
`;

class Select extends PureComponent {    
    render() {
        const { onChange, value, options } = this.props;

        const selected = options.find(item => item.id === value);

        return (
            <Button>
                {selected.value}
                <SelectTag
                    value={value}
                    onChange={onChange}
                >                    
                    {
                        options.map(item => (
                            <option 
                                value={item.id} 
                                key={item.id}                                
                            >
                                {item.value}
                            </option>
                        ))
                    }                    
                </SelectTag>
            </Button>
        );
    }
}

export default Select;