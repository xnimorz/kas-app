import React, { PureComponent } from 'react';
import ReactDropzone from 'react-dropzone';

import Image from './Image';

const ReactDropzoneImage = Image.withComponent(ReactDropzone);

class ImageLoader extends PureComponent {
    receiveFile = (file) => {
        this.props.onChange({value: file[0]});
    }

    render() {
        const { value } = this.props;

        return (
            <ReactDropzoneImage
                imageId={value}
                multiple={false}                
                accept='image/jpeg, image/png'
                onDrop={this.receiveFile}
            />
        );
    }
}

export default ImageLoader;