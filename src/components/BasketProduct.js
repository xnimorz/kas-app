import React, { PureComponent } from 'react';
import styled from 'styled-components';

import DummyBasket from './DummyBasket';
import Product from './Product';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0 20px;
`

class BasketProduct extends PureComponent {
    add = () => {
        this.props.add(this.props.product.itemId);
    }

    remove = () => {
        this.props.remove(this.props.product.itemId);
    }

    render() {
        const { product, count } = this.props;
        return (
            <Container>
                <Product product={product} plain />
                <DummyBasket count={count} add={this.add} remove={this.remove} />
            </Container>
        );
    }
}

export default BasketProduct;
