import React, { PureComponent } from 'react';
import styled from 'styled-components';
import NumberInputComponent from 'react-numberinput';

const InputComponent = styled.input`
    height: 34px;
    font-size: 18px;
`

export default class Input extends PureComponent {
    render() {
        return (
            <InputComponent {...this.props} />
        );
    }   
}

export const NumberInput = InputComponent.withComponent(NumberInputComponent);