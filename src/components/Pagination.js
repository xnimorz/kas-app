import React, { PureComponent } from 'react';
import styled, {css} from 'styled-components';
import { Link } from 'react-router';

import t from '../translations/t';

const Container = styled.div`
    margin-top: 10px;
    margin-bottom: 40px;
    display: flex;
`;

const Button = styled(Link)`    
    color: #333;   
    position: relative;
    outline: none;
    border: 1px solid #cbd1d4;
    background-color: #f9f9f9;
    font-size: 16px;         
    border-radius: 5px;    
    padding: 5px 5px;
    margin-right: 10px;
    text-decoration: none;
    display: flex;
    align-items: center;

    &:hover {
        text-decoration: none;
    }    

    ${props => props.active && css`
        background-color: #e9e9e9;
    `}
`

const FakeButton = Button.withComponent('div');

const OFFSET = 2;

class Pagination extends PureComponent {    
    renderQuickButton(page, trl, pathname) {
        return (
            <Button to={{pathname, query: {page}}} key={`quick-${page}`}>
                {trl}
            </Button>
        );
    }

    renderButtons(pagination, pathname) {
        const current = pagination.current;
        const pages = pagination.count;

        const left = current - OFFSET;
        const right = current + OFFSET;

        const start = Math.max(1, Math.min(left, left - (right - pages)));
        const stop = Math.min(pages, Math.max(right, right + (1 - left)));
        const buttons = [];

        for (let index = start; index <= stop; index++) {
            if (current === index) {
                buttons.push(
                    <FakeButton active key={index}>{index}</FakeButton>
                );
            } else {
                buttons.push(
                    <Button key={index} to={{pathname, query: {page: index}}}>{index}</Button>
                );
            }
        }

        return buttons;
    }

    render() {
        const { pagination, pathname } = this.props;

        return (
            <Container>
                {this.renderQuickButton(1, t`start`, pathname)}
                {this.renderButtons(pagination, pathname)}
                {this.renderQuickButton(pagination.count, t`end`, pathname)}
            </Container>
        );
    }
}

export default Pagination;