import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Button from './Button';

const Container = styled.div`
    display: flex;    
`;

const Value = styled.div`
    font-size: 18px;
    color: #333;
    padding: 8px 15px;
`

class DummyBasket extends PureComponent {
    render() {
        const { add, remove, count } = this.props;
        return (
            <Container>
                <Button 
                    disabled={count === 0}
                    onClick={remove}
                >
                    -
                </Button>
                <Value>
                    {count}
                </Value>
                <Button onClick={add}>
                    +
                </Button>
            </Container>
        );
    }
};

export default DummyBasket;