import React, { PureComponent } from 'react';
import styled, { css } from 'styled-components';

import { ERROR } from '../models/notifications';

const NOTIFICATION_TIMER = 3000;

const NotificationBlock = styled.div`
    min-height: 50;
    background-color: rgba(0,0,0, 0.85);
    padding: 10px 11px;
    position: relative;
    bottom: 0;
    width: 100%;
    color: #fff;
    display: block;
    box-sizing: border-box;

    ${props => props.type === ERROR && css`border: 1px solid red;`}
`;

class Notification extends PureComponent {    
    componentDidMount() {
        setTimeout(() => {
            this.props.onClose(this.props.id);
        }, NOTIFICATION_TIMER);
    }

    render() {
        const { children, type } = this.props;        

        return (
            <NotificationBlock type={type}>                
                {children}                    
            </NotificationBlock>
        );
    }
}

export default Notification;
