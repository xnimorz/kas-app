import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Notification from './Notification';
import { hide } from '../models/notifications';


const Manager = styled.div`
    position: fixed;
    bottom: 0;
    left: 0;
    display: flex;
    flex-direction: column-reverse;
    box-sizing: border-box;
    z-index: 100500;
`;

class NotificationManager extends PureComponent {        
    render() {        
        const { notifications, hide } = this.props;        

        return (
            <Manager>
                {notifications.map(({id, type, message}) =>
                    <Notification                                     
                        type={type}
                        id={id}
                        key={id}
                        onClose={hide}
                    >
                        {message}
                    </Notification>
                )}
            </Manager>
        );
    }
}

export default connect(state => {
    return {
        notifications: state.notifications,
    };
},
{
    hide,
}
)(NotificationManager);
