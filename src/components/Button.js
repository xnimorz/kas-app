import styled from 'styled-components';

export default styled.button`    
    color: #333;   
    position: relative;
    outline: none;
    border: 1px solid #cbd1d4;
    background-color: #f9f9f9;
    font-size: 16px;         
    border-radius: 5px;    
    padding: 7px 15px;
    height: 34px;                   
    box-sizing: border-box;
    text-decoration: none;
    display: inline-block;

    &:hover {
        background-color: #fafafa;
    }

    &:active {
        background-color: #fefefe;
    }
`;
