import React, { PureComponent } from 'react';
import styled, { css } from 'styled-components';
import { Link } from 'react-router';

import Image from './Image';
import numberFormatter from '../converters/numberFormatter';

const ProductItem = styled(Link)`
    padding: 20px; 
    text-decoration: none;
    display: flex;
    flex-direction: column;
    color: #333;

    ${props => props.plain && css`
        padding: 20px 0;
    `}
    
    &:hover {
        text-decoration: none;
    }

    &:visited {
        color: #333;
    }
`;

class Product extends PureComponent {
    render() {
        const { product, plain } = this.props;

        return (            
            <ProductItem 
                plain={plain}
                to={`/product/${product.itemId}`}                 
            >
                <Image imageId={product.imageId} />
                {`${product.name} — ${numberFormatter(product.price)}$`}
            </ProductItem>            
        );
    }
}

export default Product;