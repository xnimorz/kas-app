import axios from 'axios';

import { DIRTY, CLEAR } from '../constants/modelStates';
import { processError } from './errors';
import { collect } from './collectMiddleware';

const API_FETCH = 'basket/api/fetch';
const API_ADD = 'basket/api/add';
const API_REMOVE = 'basket/api/remove';
const RECEIVE = 'basket/receive';
const RESET = 'basket/reset';

export const receiveBasket = (basket) => ({
    type: RECEIVE,
    payload: basket,
});

export const basketMiddleware = ({dispatch, getState}) => next => async action => {
    const apiAction = action[API_FETCH] || action[API_ADD] || action[API_REMOVE];    

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    let result;
    let method = 'get';
    if (action[API_ADD]) {
        method = 'put';
    }
    if (action[API_REMOVE]) {
        method = 'delete';
    }
    try {
        result = await axios[method](`/basket/${apiAction.id ? apiAction.id : ''}`);
    } catch(e) {
        dispatch(processError(e));
        return;
    }

    try {
        dispatch(collect(result.data));
    } catch(e) {
        dispatch(processError(e));
    }    
}

export const loadBasket = () => ({    
    [API_FETCH]: {},
});

export const addToBasket = (id) => ({    
    [API_ADD]: {id},
});

export const removeFromBasket = (id) => ({    
    [API_REMOVE]: {id},
});

export default function basket(state = {status: DIRTY}, action = {}) {
    switch (action.type) {
        case RECEIVE: {
            return {
                status: CLEAR,
                data: action.payload,
            };
        }

        case RESET: {
            return {
                status: DIRTY,
            };
        }

        default: 
            return state;
    }
}