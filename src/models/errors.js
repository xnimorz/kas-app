/**
 *  Так как ошибки в приложении +\- единые, то создадим единый middleware для этого
 */
import t from '../translations/t';
import { showError } from './notifications';

const ERROR = 'error';

export const processError = (e) => ({
    [ERROR]: e,
}) ;

export const errorMiddleware = ({dispatch}) => next => action => {
    const errorAction = action[ERROR];

    if (typeof errorAction === 'undefined') { 
        return next(action);
    }

    if (!navigator.onLine) {
        return dispatch(showError(t`check.connection`));
    }

    if (errorAction.response) {
        if (errorAction.response.status === 400) {
            //  Переданы неверные параметры
            return dispatch(showError(t`something.went.wrong`));
        }

        if (errorAction.response.status === 404) {
            return dispatch(showError(t`not.found`));
        }

        if (errorAction.response.status === 404) {
            return dispatch(showError(t`not.found`));
        }
        
    }

    // Произошла кастомная ошибка, например при рендеринге. В dev режиме хотим получить стек-трейс
    dispatch(showError(t`something.went.wrong`, errorAction));
}