import { receiveCategories } from './categories';
import { receiveGoods } from './goods';
import { receiveProduct } from './product';
import { receiveBasket } from './basket';
import { processError } from './errors';

const API_COLLECT = 'api_collect';

const logic = [
    {
        field: 'basket',
        action: receiveBasket,
    },
    {
        field: 'categories',
        action: receiveCategories,
    },
    {
        field: 'goods',
        action: receiveGoods,
    },
    {
        field: 'product',
        action: receiveProduct,
    },    
];

export const collectMiddleware = ({dispatch}) => next => action => {
    const apiAction = action[API_COLLECT];
    
    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    const result = [];
    
    logic.forEach(item => {
        if (apiAction[item.field]) {
            result.push(item.action(apiAction[item.field]));
        }
    })

    try {
        dispatch(result);
    } catch(e) {
        dispatch(processError(e));
    }
        
};

export const collect = (data) => ({
    [API_COLLECT]: data,
});
