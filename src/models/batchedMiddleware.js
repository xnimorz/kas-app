import { batch } from '../store/batchingReducer';

export const batchedMiddleware = ({dispatch}) => next => action => {
    if (!Array.isArray(action)) {
        return next(action);
    }

    dispatch(batch(action));
};