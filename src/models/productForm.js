/**
 * Отдельно вынесли редактирование формы, чтобы в основном редьюсере product держать "закоммиченные" данные.
 * Так как в приложении только одна форма, делаем ее специфичной. Не создаем излишние абстракции для конкретной задачи. 
 * Таким образом, при росте числа форм мы сможем получить явный список задач формы и выделить абстракции. 
 */

 /**
 * Важный момент: 
 * В тз нет информации о валидации данных, поэтому по умолчанию не ограничиваем длины данных и прочее. 
 * В противном случае, при клике пользователя на кнопку редактировать нужно пойти на сервер и запросить правила валидации. 
 * Держать информацию о валидации на клиенте — не лучший путь, так как клиентская и серверная валидация в какой-то момент разойдутся при развитии приложения.
 */
import axios from 'axios';

import { show } from './notifications';
import { processError } from './errors';
import t from '../translations/t';
import { CLEAR, DIRTY } from '../constants/modelStates';

const API_LOAD_IMAGE = 'productForm/api/load/image';

const CREATE = 'productForm/create';
const RECEIVE_IMAGE = 'productForm/receive/image';
const RESET = 'productForm/reset';
const WRITE = 'productForm/write';

export const productFormLoadImageMiddleware = ({dispatch}) => next => async action => {
    const apiAction = action[API_LOAD_IMAGE];        

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    const data = new FormData();
    data.append('file', apiAction.file);

    let result;
    try {
        result = await axios.post('/image', data);
    } catch(e) {
        dispatch(processError(e));
        return;
    }

    try {        
        dispatch([
            {
                type: RECEIVE_IMAGE,
                payload: result.data,
            },
            show(t`image.uploaded`),
        ]);
    } catch(e) {        
        dispatch(processError(e));
    }    
}

export const loadImage = (file) => ({    
    [API_LOAD_IMAGE]: {
        file,
    },
});

export const write = (field, value) => {
    if (field === 'imageId') {
        return loadImage(value);
    }
    
    return {
        type: WRITE,
        payload: {field, value},
    };
}

export const resetForm = () => ({
    type: RESET,
});

export const createForm = (product) => ({
    type: CREATE,
    payload: product,
});

export default function productForm(state = {status: DIRTY}, action = {}) {
    switch (action.type) {
        case CREATE: {
            return {
                status: CLEAR,
                data: {...action.payload},
            };
        }

        case WRITE: {
            return {
                ...state,
                data: {
                    ...state.data, 
                    [action.payload.field]: action.payload.value,
                },
            };
        }

        case RESET: {
            return {
                status: DIRTY,
            };
        }

        case RECEIVE_IMAGE: {
            return {
                ...state,
                data: {...state.data, imageId: action.payload.id},
            };
        }

        default: 
            return state;
    }
}