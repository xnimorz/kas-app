const SHOW = 'notification/show';
const HIDE = 'notification/hide';

export const ERROR = 'error';
export const INFO = 'info';

export const showError = (message, e) => {
    /**
     * Если произошла ошибка, выбрасываем ее в консоль, причем выбрасываем только в dev режиме и асинхронно, чтобы не взорвать runtime.
     */
    
    if (e && process && process.env && process.env.NODE_ENV === 'development') {
        setTimeout(() => {throw e}, 0);
    }

    return {
        type: SHOW,
        payload: {
            type: ERROR,
            message,
        },
    };
}

export const hide = (id) => ({
    type: HIDE,
    payload: {id},
});

export const show = (message) => ({
    type: SHOW,
    payload: {
        type: INFO,
        message,
    },
});

export default function notifications(state = [], action = {}) {
    switch (action.type) {
        case SHOW: 
            return [
                {
                    id: state.length > 0 ? state[0].id + 1 : 1,
                    message: action.payload.message,
                    type: action.payload.type,                    
                },
                ...state,
            ];        

        case HIDE: 
            return state.filter(notification => notification.id !== action.payload.id);
        
        default: 
            return state;
        
    }
};