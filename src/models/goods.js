import axios from 'axios';

import { DIRTY, CLEAR } from '../constants/modelStates';
import { processError } from './errors';
import { collect } from './collectMiddleware';

const API_FETCH = 'goods/api/fetch';
const RECEIVE = 'goods/receive';
const RESET = 'goods/reset';

export const goodsMiddleware = ({dispatch}) => next => async action => {
    const apiAction = action[API_FETCH];

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    let result;
    try {
        result = await axios.get(`/goods/${apiAction.category || ''}`, {params: apiAction.params});
    } catch(e) {
        dispatch(processError(e));
        return;
    }

    try {
        dispatch(collect(result.data));
    } catch(e) {
        // ошибки при рендеринге
        dispatch(processError(e));
    }    

}

export const loadGoods = (category, page = 1) => ({    
    [API_FETCH]: {
        category,
        params: {page},
    },
});

export const receiveGoods = (goods) => ({
    type: RECEIVE,
    payload: goods,
});

export const resetGoods = () => ({
    type: RESET,
});

export default function goods(state = {status: DIRTY}, action = {}) {
    switch (action.type) {
        case RECEIVE: {
            return {
                status: CLEAR,
                data: action.payload,
            };
        }

        case RESET: {
            return {
                status: DIRTY,
            };
        }

        default: 
            return state;
    }
}