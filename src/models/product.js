import axios from 'axios';

import { DIRTY, CLEAR } from '../constants/modelStates';
import { show } from './notifications';
import { processError } from './errors';
import { resetForm } from './productForm';
import t from '../translations/t';
import { collect } from './collectMiddleware';

const API_FETCH = 'product/api/fetch';
const API_EDIT = 'product/api/edit'
const RECEIVE = 'product/receive';
const RESET = 'product/reset';

export const productFetchMiddleware = ({dispatch}) => next => async action => {
    const apiAction = action[API_FETCH];        

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    let result;
    try {
        result = await axios.get(`/product/${apiAction.id}`);
    } catch(e) {
        dispatch(processError(e));
    }

    try {
        dispatch(collect(result.data));
    } catch(e) {        
        dispatch(processError(e));
    }    
}

export const productEditMiddleware = ({dispatch, getState}) => next => async action => {
    const apiAction = action[API_EDIT];        

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    let result;
    try {
        result = await axios.put(`/product/${apiAction.id}`, getState().productForm.data);
    } catch(e) {
        dispatch(processError(e));
        return;
    }

    try {        
        dispatch([
            receiveProduct(result.data),
            show(t`product.edited`),
            resetForm(),
        ]);
    } catch(e) {        
        dispatch(processError(e));
    }    
}

export const loadProduct = (id) => ({    
    [API_FETCH]: {
        id,
    },
});

export const receiveProduct = (product) => ({
    type: RECEIVE,
    payload: product,
});

export const resetProduct = () => ({
    type: RESET,
});

export const editProduct = (id) => ({
    [API_EDIT]: {
        id,         
    },
});

export default function product(state = {status: DIRTY}, action = {}) {
    switch (action.type) {
        case RECEIVE: {
            return {
                status: CLEAR,
                data: action.payload,
            };
        }

        case RESET: {
            return {
                status: DIRTY,
            };
        }

        default: 
            return state;
    }
}