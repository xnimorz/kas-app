import axios from 'axios';

import { DIRTY, CLEAR } from '../constants/modelStates';
import { processError } from './errors';
import { collect } from './collectMiddleware';

const API_FETCH = 'categories/api/fetch';
const RECEIVE = 'categories/receive';
const RESET = 'categories/reset';

export const receiveCategories = (categories) => ({
    type: RECEIVE,
    payload: categories,
});

export const categoriesMiddleware = ({dispatch, getState}) => next => async action => {
    const apiAction = action[API_FETCH];

    if (typeof apiAction === 'undefined') { 
        return next(action);
    }

    let result;
    try {
        result = await axios.get('/categories');
    } catch(e) {
        dispatch(processError(e));
        return;
    }

    try {
        dispatch(collect(result.data));
    } catch(e) {
        dispatch(processError(e));
    }
    
}

export const loadCategories = () => ({    
    [API_FETCH]: {},
});

/**
 * Здесь и далее считаем по коду, что пользователь уходя из страницы — инвалидирует состояние текущих моделей, 
 * так как он может открыть страницу и перейти на следующую по истечении времени. 
 * Поэтому на каждой следующей странице запрашиваем объект снова, чтобы актуализировать состояние. 
 * Если мы так не хотим делать (и считаем данные актуальными), то убираем reset вызовы из componentWillUnmount react lifecycle методов. 
 */
export const resetCategories = () => ({
    type: RESET,
});

export default function categories(state = {status: DIRTY}, action = {}) {
    switch (action.type) {
        case RECEIVE: {
            return {
                status: CLEAR,
                data: action.payload,
            };
        }

        case RESET: {
            return {
                status: DIRTY,
            };
        }

        default: 
            return state;
    }
}