import React from 'react';

import t from '../translations/t';

export default function() {
    return (
        <div>
            {t`not.found`}
        </div>
    );
}