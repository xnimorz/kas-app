import React, { PureComponent } from 'react';

import Header from '../components/Header';
import NotificationManager from '../components/NotificationManager';

class App extends PureComponent {
    render() {
        return (
            <div>
                <NotificationManager />
                <Header />
                {this.props.children}
            </div>
        );
    }
}

export default App;