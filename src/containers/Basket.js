import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import Content from '../components/Content';
import BasketList from '../components/BasketList';
import { CLEAR } from '../constants/modelStates';
import { loadBasket } from '../models/basket';
import { resetGoods } from '../models/goods';

class Basket extends PureComponent {
    componentDidMount() {
        this.props.loadBasket();
    }    

    componentWillUnmount() {
        this.props.resetGoods();
    }

    render() {
        if (this.props.basket.status !== CLEAR || this.props.goods.status !== CLEAR) {
            return null;
        }

        return (
            <Content>
                <BasketList />                
            </Content>
        );
    }
}

export default connect(state => ({
    basket: state.basket,      
    goods: state.goods,      
}), {
    loadBasket, 
    resetGoods,   
})(Basket);