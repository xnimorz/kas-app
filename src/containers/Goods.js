import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import Pagination from '../components/Pagination';
import GoodsList from '../components/GoodsList';
import { CLEAR } from '../constants/modelStates';
import { loadGoods, resetGoods } from '../models/goods';
import Content from '../components/Content';

class Goods extends PureComponent {
    componentDidMount() {
        this.props.loadGoods(this.props.params.categoryId, this.props.location.query.page);
    }

    componentDidUpdate(prevProps) {
        /**
         * Вторая проверка нужна, так как, если пользователь использует жесты назад\вперед для перехода по страницам, 
         * location изменяет свое состояние раньше, чем роуты, и поэтому componentDidUpdate вызывается лишний раз.
         */
        if (prevProps.location !== this.props.location && this.props.location.pathname.startsWith('/goods')) {            
            this.props.resetGoods();
            this.props.loadGoods(this.props.params.categoryId, this.props.location.query.page);
        }
    }

    componentWillUnmount() {
        this.props.resetGoods();
    }

    render() {
        if (this.props.goods.status !== CLEAR) {
            return null;
        }

        return (
            <Content>
                <GoodsList 
                    goods={this.props.goods.data.items} 
                />
                <Pagination 
                    pathname={this.props.location.pathname}
                    pagination={this.props.goods.data.pagination}
                />
            </Content>
        );
    }
}

export default connect(state => ({
    goods: state.goods,  
    location: state.routing.location,  
}), {
    loadGoods,
    resetGoods,
})(Goods);