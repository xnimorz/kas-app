import React, { Component } from 'react';
import { replace } from 'react-router-redux';
import { connect } from 'react-redux';

class MainPage extends Component {
    componentDidMount() {
        this.props.replace('/goods');
    }

    render() {
        return (
            <div>
            </div>
        );
    }
}

export default connect(null, {replace})(MainPage);
