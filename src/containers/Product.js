import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import Content from '../components/Content';
import EditableProduct from '../components/EditableProduct';
import { CLEAR } from '../constants/modelStates';
import { loadProduct, resetProduct } from '../models/product';

class Product extends PureComponent {
    componentDidMount() {
        this.props.loadProduct(this.props.params.id);
    }    

    componentWillUnmount() {
        this.props.resetProduct();
    }

    render() {
        if (this.props.product.status !== CLEAR) {
            return null;
        }

        return (
            <Content>
                <EditableProduct />                
            </Content>
        );
    }
}

export default connect(state => ({
    product: state.product,      
}), {
    loadProduct,
    resetProduct,
})(Product);