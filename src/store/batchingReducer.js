export const BATCH = 'store/batch';

export const batch = (actions) => {
    return {
        payload: actions,
        type: BATCH,
    };
};

export default function enableBatching(rootReducer) {
    return function batchingReducer(state, action) {
        if (action.type === BATCH) {
            return action.payload.reduce(rootReducer, state);
        }

        return rootReducer(state, action);
    };
}
