import { createStore, applyMiddleware, combineReducers } from 'redux';
import { syncHistory, routeReducer } from 'react-router-redux';
import { browserHistory } from 'react-router';

import enableBatching from './batchingReducer';
import { batchedMiddleware } from '../models/batchedMiddleware';
import { collectMiddleware } from '../models/collectMiddleware';
import categories, { categoriesMiddleware } from '../models/categories';
import goods, { goodsMiddleware } from '../models/goods';
import product, { productEditMiddleware, productFetchMiddleware } from '../models/product';
import productForm, { productFormLoadImageMiddleware } from '../models/productForm';
import basket, { basketMiddleware } from '../models/basket';
import { errorMiddleware } from '../models/errors';
import notifications from '../models/notifications';
import logger from './batchedLogger';

const reduxRouterMiddleware = syncHistory(browserHistory);

const middlewares = [
    reduxRouterMiddleware, 
    errorMiddleware, 
    productFormLoadImageMiddleware, 
    productEditMiddleware,
    productFetchMiddleware,
    goodsMiddleware,
    categoriesMiddleware,
    basketMiddleware,
    collectMiddleware,
    batchedMiddleware, 
    logger
];

export default function configureStore() {
    const store = createStore(enableBatching(
        combineReducers({
            routing: routeReducer,
            categories,
            goods,
            basket,
            product,
            productForm,            
            notifications,
        })), 
        applyMiddleware(...middlewares)
    );    
    return store;
}
