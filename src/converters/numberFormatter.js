export default (value) =>
    String(value)
        .replace(/\s/g, '')
        .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, text => text + ' ');    