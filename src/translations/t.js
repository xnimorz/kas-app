import keys from './keys';

export default function(strings) {
    const key = strings.raw[0];
    const trl = keys[key];
    if (!trl) {
        console.error(`There is not trl for key: ${key}`);        
    }
    return trl;
}