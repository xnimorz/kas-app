import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import './index.css';
import './fetcher/config';

import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';

import App from './containers/App';
import Basket from './containers/Basket';
import MainPage from './containers/MainPage';
import Goods from './containers/Goods';
import Product from './containers/Product';
import NotFound from './containers/NotFound';

const store = configureStore();

ReactDOM.render(
    (
        <Provider store={store}>
            <Router history={browserHistory}>
                <Route path='/' component={App}>
                    <IndexRoute component={MainPage} /> 
                    <Route path='goods' component={Goods} />
                    <Route path='goods/:categoryId' component={Goods} />
                    <Route path='product/:id' component={Product} />                    
                    <Route path='basket' component={Basket} />
                </Route>
                <Route path='*' component={NotFound} />
            </Router>
        </Provider>
    ), document.getElementById('root')
);

registerServiceWorker();
